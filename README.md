# Scandiweb TaskTwo Module

Changes the color of all buttons 

### How to install

The instalation is plug and play all you need to do is:

##### AUTOMATIC WAY

- composer config repositories.scandiweb/task-two git git@bitbucket.org:thiagocaldeiralima/scandiweb_tasktwo.git
- composer require scandiweb/task-two:dev-master

##### MANUAL WAY

-  copy the code to the `app/code` folder
-  run: `bin/magento setup:upgrade` to install the module

### How it works

Just run the command
--color = hexadecimal color 
--store_id = id of the store

`bin/magento scandiweb:color-change --color 000000 --store_id 1`


### The expected result

The following content just be added to the html header

`<!--CONTENT--><style>.action.primary {background: #000000 !important;border: 1px solid #000000 !important;}</style><!--/CONTENT-->`
