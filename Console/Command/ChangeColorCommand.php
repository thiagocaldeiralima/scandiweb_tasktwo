<?php
/**
 * @author Scandiweb Team
 * @copyright Copyright © Scandiweb (https://scandiweb.com)
 */
namespace Scandiweb\TaskTwo\Console\Command;

use Scandiweb\TaskTwo\Service\ConfigWriter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ChangeColorCommand extends Command
{

    const STORE = 'store_id';
    const COLOR = 'color';

    /**
     * @var ConfigWriter
     */
    private $configWriter;

    /**
     * ChangeColorCommand constructor.
     * @param ConfigWriter $configWriter
     * @param string|null $name
     */
    public function __construct(
        ConfigWriter $configWriter,
        string $name = null
    ) {
        $this->configWriter = $configWriter;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('scandiweb:color-change');
        $this->setDescription('Changes the button colour from a given store id');
        $this->addOption(
            self::COLOR,
            null,
            InputOption::VALUE_REQUIRED,
            'Color'
        )->addOption(
            self::STORE,
            null,
            InputOption::VALUE_REQUIRED,
            'Store Id'
        );
        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $color = $input->getOption(self::COLOR);
        $store = $input->getOption(self::STORE);
        try {
            $output->writeln("<info>Setting the color {$color} to the store_id {$store}...</info>");
            $this->configWriter->setData($color, $store);
            $output->writeln("<info>The button color was updated to {$color}</info>");
            $output->writeln("<info>Cleaning Cache</info>");
            $this->flushCache($output);
            $output->writeln("<info>All Set</info>");
        } catch (\Exception $e) {
            $output->writeln("<error>{$e->getMessage()}</error>");
            $output->writeln("<comment>Type `bin/magento scandiweb:color-change --help` to see the options</comment>");
        }
        return 1;
    }

    /**
     * @param OutputInterface $output
     * @throws \Exception
     */
    private function flushCache(OutputInterface $output): void
    {
        $arguments = new ArrayInput(['command' => 'cache:flush', 'types' => ['config']]);
        $this->getApplication()->find('cache:flush')->run($arguments, $output);
    }
}
