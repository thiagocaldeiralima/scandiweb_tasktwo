<?php
/**
 * @author Scandiweb Team
 * @copyright Copyright © Scandiweb (https://scandiweb.com)
 */
namespace Scandiweb\TaskTwo\Service;

use Composer\Config\ConfigSourceInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class ConfigWriter
{
    const COLOR_REGEX = '/([[:xdigit:]]{3}){1,2}\b/';
    const PATH = 'design/head/includes';
    const SCOPE = 'stores';
    const TEMPLATE = "<!--CONTENT--><style>.action.primary {background: #%s !important;border: 1px solid #%s !important;}</style><!--/CONTENT-->";

    /**
     * @var WriterInterface
     */
    protected $configWriter;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var ConfigSourceInterface
     */
    private $scopeConfig;

    /**
     * ConfigWriter constructor.
     * @param WriterInterface $configWriter
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        WriterInterface $configWriter,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->configWriter = $configWriter;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * @param $color
     * @param $scopeId
     * @throws NoSuchEntityException
     * @throws \Exception
     */
    public function setData($color, $scopeId)
    {
        if (!preg_match(self::COLOR_REGEX, $color)) {
            throw new \Exception("Invalid color format: {$color}");
        }
        $store = $this->storeManager->getStore($scopeId); // checks if store exists
        $actualContent = $this->getConfigValue($store->getId());
        $newContent = sprintf(self::TEMPLATE, $color, $color);
        $newContent = $this->updateContent($actualContent, $newContent);
        $this->configWriter->save(self::PATH, $newContent, self::SCOPE, $scopeId);
    }

    /**
     * Get the actual config value for design/head/includes
     *
     * @param $store
     * @return string
     */
    private function getConfigValue($store): string
    {
        return (string) $this->scopeConfig->getValue(self::PATH, ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Update the value
     *
     * @param string $actualContent
     * @param string $newContent
     * @return string|string[]
     */
    private function updateContent(string $actualContent, string $newContent)
    {
        $content = $actualContent;
        preg_match('/<!--CONTENT-->(.*?)<!--\/CONTENT-->/s', $actualContent, $match);
        $content = ($match)
            ? str_replace($match[0], $newContent, $content)
            : $actualContent . $newContent;
        return $content;
    }
}
